def add(x, y):
    return x + y


def sub(x, y):
    return x - y


def multiplication(x, y):
    return x * y


def test_func():
    assert add(1, 2) == 3
    assert sub(1, 2) == -1
    assert multiplication(1, 2) == 2
